<?php

namespace OpenGraph\Facebook;

abstract class BaseObject
{
    const METHOD_GET  = 'GET';
    const METHOD_POST = 'POST';

    /**
     * @var string
     */
    protected $id;

    /**
     * @var \BaseFacebook
     */
    protected $facebookApi;

    /**
     * @var string
     */
    protected $accessToken;

    /**
     * @param \BaseFacebook $facebookApi
     * @param string $pageId
     * @param string $accessToken
     */
    public function __construct(\BaseFacebook $facebookApi, $id, array $bootstrapData = array())
    {
        $this->facebookApi = $facebookApi;
        $this->id = $id;

        $this->initialize($bootstrapData);
    }

    public function getId()
    {
        return $this->id;
    }

    protected function initialize(array $bootstrapData)
    {
    }

    protected function apiCall($resource, $method = self::METHOD_GET, $params = array())
    {
        $uri = sprintf("%s/%s", $this->id, $resource);
        if (!empty($params)) {
            $uri .= '?' . http_build_query($params);
        }

        return $this->facebookApi->api($uri, $method);
    }
}