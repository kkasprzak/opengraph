<?php

namespace OpenGraph\Facebook\Object;

use OpenGraph\Facebook\BaseObject;

class Post extends BaseObject
{
    private $type;
    private $message;
    private $object_id;
    private $created_time;
    private $from;

    protected function initialize(array $data)
    {
        foreach (array('type', 'message', 'object_id') as $fieldName) {
            if (empty($data[$fieldName])) {
                continue;
            }
            $this->$fieldName = $data[$fieldName];
        }

        if (false == empty($data['created_time'])) {
            $this->created_time = strtotime($data['created_time']);
        }

        if (false == empty($data['from'])) {
            if (empty($data['from']['category'])) {
                $this->from = new User($this->facebookApi, $data['from']['id'], $data['from']);
            } else {
                $this->from = new Page($this->facebookApi, $data['from']['id'], $data['from']);
            }
        }
    }

    public function getType()
    {
        return $this->type;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function hasPhoto()
    {
        return 'photo' == $this->getType();
    }

    public function getObjectId()
    {
        return $this->object_id;
    }

    public function getCreatedTime()
    {
        return $this->created_time;
    }

    /**
     * @return mixed Page|User
     */
    public function getFrom()
    {
        return $this->from;
    }
}