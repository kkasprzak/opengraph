<?php

namespace OpenGraph\Facebook\Object;

use OpenGraph\Facebook\BaseObject;

class Album extends BaseObject
{
    private $name;
    private $type;

    protected function initialize(array $data)
    {
        foreach (array('name', 'type') as $fieldName) {
            if (empty($data[$fieldName])) {
                continue;
            }
            $this->$fieldName = $data[$fieldName];
        }
    }

    public function getPhotos(array $params = array())
    {
        $response = $this->apiCall('photos', self::METHOD_GET, $params);
        if (empty($response['data'])) {
            return array();
        }

        $photos = array();
        foreach ($response['data'] as $item) {
            $photos[] = new Photo($this->facebookApi, $item['id'], $item);
        }

        return $photos;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->type;
    }

    public function isWallAlbum()
    {
        return "wall" == $this->getType();
    }
}