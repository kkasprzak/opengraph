<?php

namespace OpenGraph\Facebook\Object;

use OpenGraph\Facebook\BaseObject;

class User extends BaseObject
{
    private $name;

    protected function initialize(array $data)
    {
        foreach (array('name') as $fieldName) {
            if (empty($data[$fieldName])) {
                continue;
            }
            $this->$fieldName = $data[$fieldName];
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getFriends(array $params = array())
    {
        $response = $this->apiCall('friends', self::METHOD_GET, $params);
        if (empty($response['data'])) {
            return array();
        }

        $friends = array();
        foreach ($response['data'] as $item) {
            $friends[] = new User($this->facebookApi, $item['id'], $item);
        }

        return $friends;
    }
}