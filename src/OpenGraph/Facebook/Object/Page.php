<?php

namespace OpenGraph\Facebook\Object;

use OpenGraph\Facebook\BaseObject;

class Page extends BaseObject
{
    private $name;

    protected function initialize(array $data)
    {
        foreach (array('name') as $fieldName) {
            if (empty($data[$fieldName])) {
                continue;
            }
            $this->$fieldName = $data[$fieldName];
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAlbums(array $params = array())
    {
        $response = $this->apiCall('albums', self::METHOD_GET, $params);
        if (empty($response['data'])) {
            return array();
        }

        $albums = array();
        foreach ($response['data'] as $item) {
            $albums[] = new Album($this->facebookApi, $item['id'], $item);
        }

        return $albums;
    }

    public function getFeed(array $params = array())
    {
        $response = $this->apiCall('feed', self::METHOD_GET, $params);
        if (empty($response['data'])) {
            return array();
        }

        $posts = array();
        foreach ($response['data'] as $item) {
            $posts[] = new Post($this->facebookApi, $item['id'], $item);
        }

        return $posts;
    }
}